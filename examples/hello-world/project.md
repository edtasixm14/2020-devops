# Hello-world jenkins example
For this we will need a git repository with the code to create this basic example. Ex: https://github.com/welharrak/hello-world

## Steps
- First of all, we have to create a new job, and choose the **Pipeline** option:  
![NewJob](./../../aux/hello1.png)

- [Optional]**General**: We can write a description of the project and the Git repository.  
![General](./../../aux/hello2.png)

- **Build Triggers**: In this section, we choose **Poll SCM** and write the following schedule: ``` * * * * *```. This is going to tell to he Job that every minute have to see if there are changes in the Git repository, and if there are changes, have to do the build.  
![Build](./../../aux/hello3.png)

- **Pipeline**: Here we will set the source of the Pipeline (Jenkinsfile), we have to choose Git as SCM and write the **Git url**, not the web url, this is very important.
![Pipeline](./../../aux/hello4.png)

- And that's it! We have created a basic project. Now we are going to see the process.

## Process (Pipeline)
- The Jenkinsfile is the Jenkins Pipeline, and contains the stages of the build and the steps that have to do to complete the build.

- In this case, we have the following [Jenkinsfile](./Jenkinsfile):
1. The first stage it's only to clean the server of unused and useless images.
```
stage('Delete all unused images') {
      steps {
        sh 'echo "y" | docker system prune'
      }
    }
```

2. The second stage, stops the running container that use the image created before, if it's exists.
```
stage('Stop httpserver running containers') {
     steps {
       sh 'docker stop httpserver || echo "there is no httpserver running container"'
     }
   }
```

3. The third stage, we build the docker image with the Dockerfile that we have in the repository.
```
stage('Build image') {
      steps {
        sh 'docker build -t hello-walid:latest .'
      }
    }
```

4. And finally, we run a docker container using the image created, binding the container port 80 to the host port 80, simulating that the host is the web server.
```
stage('Run the image created') {
      steps {
        sh 'docker run --rm --name httpserver -h httpserver -p 80:80 -d hello-walid:latest'
      }
    }
```
