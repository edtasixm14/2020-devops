# LDAP server jenkins example
For this we will need a git repository with the code to create this basic example. Ex: https://github.com/welharrak/ldap

## Steps
- First of all, we have to create a new job, and choose the **Pipeline** option:  
![NewJob](./../../aux/ldap1.png)

- [Optional]**General**: We can write a description of the project and the Git repository.  
![General](./../../aux/ldap2.png)

- **Build Triggers**: In this section, we choose **Poll SCM** and write the following schedule: ``` * * * * *```. This is going to tell to he Job that every minute have to see if there are changes in the Git repository, and if there are changes, have to do the build it.  
![Build](./../../aux/ldap3.png)

- **Pipeline**: Here we will set the source of the Pipeline (Jenkinsfile), we have to choose Git as SCM and write the **Git url**, not the web url, this is very important.
![Pipeline](./../../aux/ldap4.png)

- And that's it! We have created a LdapServer project. Now we are going to see the process.

## Process (Pipeline)
- The Jenkinsfile is the Jenkins Pipeline, and contains the stages of the build and the steps that have to do to complete the build.

- In this case, we have the following [Jenkinsfile](./Jenkinsfile):
1. The first stage it's only to clean the server of unused and useless images.
```
stage('Delete all unused images') {
      steps {
        sh 'echo "y" | docker system prune'
      }
    }
```

2. The second stage, stops the running container that use the image created before, if it's exists.
```
stage('Stop ldapserver running containers') {
      steps {
        sh 'docker stop ldapserver || echo "there is no ldapserver running container"'
      }
    }
```

3. The third stage, we build the docker image with the Dockerfile that we have in the repository.
```
stage('Build image') {
      steps {
        sh 'docker build -t ldapserver:latest .'
      }
    }
```

4. And finally, we run a docker container using the image created, binding the container port 389 to the host port 389, simulating that the host is the LDAP server.
```
stage('Run the image created') {
      steps {
        sh 'docker run --rm --name ldapserver -h ldapserver -p 389:389 -d ldapserver:latest'
      }
    }
```
