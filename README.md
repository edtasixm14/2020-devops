# Proyecto CI/DevOps EdT 2020 - HISX2

## Propósito del proyecto
El objetivo es crear un entorno de trabajo, en este caso con docker, y hacer que en este entorno se trabaje de manera automatizada, es decir, un entorno de lo más eficiente y automatizado, que permita la detección de errores rápidamente y por tanto, una optimización del trabajo y el  tiempo.

## Integración continua
- Modelo informático
-  Consiste en hacer integraciones automáticas para así poder detectar fallos cuanto antes
- **Integración** --> Compilación y ejecución de pruebas

## DevOps
- És una **práctica** de ingeniería de software
- **Objetivo** --> unificar el desarrollo de software (Dev) y la operación del software (Ops)

## Conceptos importantes
- **Pipeline**: conjunto de instrucciones del proceso que sige una aplicación desde el repositorio de control de versiones hasta que llega a producción
- **Jenkinsfile**: fichero donde se establece el Pipeline

## Jenkinsfile
- El pipeline del proyecto se declara en un fichero, se almacena y se versiona junto con el código en un fichero comúnmente llamado Jenkinsfile. En este fichero definimos principalmente las fases (stages) de que las consta nuestro flujo.

- Un ejemplo de Jenkinsfile sencillo, donde crea y pone en marcha una imagen a partir de un Dockerfile: [Jenkinsfile](./examples/hello-world/Jenkinsfile)

## Proceso (Fedora 29+)

### Prerequisitos
- Instalar Jenkins i Docker en nuestro host de 'pruebas', en este caso lo instalaré en una máquina AWS, pero és el mismo proceso en todos los sistemas Fedora 29 o superior.

- [Instalación Jenkins](./develop/jenkins/install.md)

- [Instalación Docker](./develop/docker/install.md)

- Una vez instalados Jenkins i Docker, damos permiso a jenkins para poder trabajar con Docker i reiniciamos el servicio de Jenkins:
```
sudo usermod -aG docker jenkins

sudo systemctl restart jenkins
```

- Un repositorio **GitHub** donde dispongamos de Dockerfile y sus configuraciónes. Por ejemplo: https://github.com/welharrak/hello-world

### Ejemplos
- [Hello World](./examples/hello-world/project.md)

- [LdapServer](./examples/ldapserver/project.md)

- [Maven](./examples/maven/project.md)
